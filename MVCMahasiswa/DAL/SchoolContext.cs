﻿using MVCMahasiswa.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MVCMahasiswa.DAL
{
    public class SchoolContext :DbContext
    {
        public SchoolContext() : base("SchoolContext")
        {
        }

        public DbSet<Mahasiswa> Mahasiswas { get; set; }
        public DbSet<DaftarNilai> DaftarNilais { get; set; }
        public DbSet<MataKuliah> MataKuliahs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}