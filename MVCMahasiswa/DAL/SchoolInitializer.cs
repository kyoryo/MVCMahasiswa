﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MVCMahasiswa.Models;

namespace MVCMahasiswa.DAL
{
    public class SchoolInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<SchoolContext>
    {
        protected override void Seed(SchoolContext context)
        {
            var mahasiswas = new List<Mahasiswa>
            {
                new Mahasiswa{NamaMahasiswa="Kizaki Yuria", TglMasuk=DateTime.Parse("2015-05-01")},
                new Mahasiswa{NamaMahasiswa="Oshima Ryoka", TglMasuk=DateTime.Parse("2015-05-01")},
                new Mahasiswa{NamaMahasiswa="Kashiwagi Yuki", TglMasuk=DateTime.Parse("2015-05-01")},
                new Mahasiswa{NamaMahasiswa="Watanabe Mayu", TglMasuk=DateTime.Parse("2015-05-01")},
                new Mahasiswa{NamaMahasiswa="Watanabe Miyuki ", TglMasuk=DateTime.Parse("2015-05-01")},
                new Mahasiswa{NamaMahasiswa="Kato Rena", TglMasuk=DateTime.Parse("2015-05-01")},
                new Mahasiswa{NamaMahasiswa="Tanabe Miku", TglMasuk=DateTime.Parse("2015-05-01")},
                new Mahasiswa{NamaMahasiswa="Kobayashi Kana", TglMasuk=DateTime.Parse("2015-05-01")}
            };
            mahasiswas.ForEach(s => context.Mahasiswas.Add(s));
            context.SaveChanges();

            var matakuliahs = new List<MataKuliah>{
                new MataKuliah{MataKuliahID=1050,NamaMataKuliah="Kalkulus",Bobot=3,},
                new MataKuliah{MataKuliahID=2050,NamaMataKuliah="Struktur Data",Bobot=4,},
                new MataKuliah{MataKuliahID=3050,NamaMataKuliah="Artificial Intelligence",Bobot=4,},
                new MataKuliah{MataKuliahID=4050,NamaMataKuliah="Bahasa Ingris",Bobot=2,},
                new MataKuliah{MataKuliahID=5050,NamaMataKuliah="Kewarganegaraan",Bobot=2,}
            };
            matakuliahs.ForEach(s => context.MataKuliahs.Add(s));
            context.SaveChanges();

            var daftarnilais = new List<DaftarNilai>{
                new DaftarNilai{MahasiswaID=1,MataKuliahID=1050,IndexNilai=IndexNilai.A},
                new DaftarNilai{MahasiswaID=1,MataKuliahID=2050,IndexNilai=IndexNilai.C},
                new DaftarNilai{MahasiswaID=1,MataKuliahID=3050,IndexNilai=IndexNilai.D},
                new DaftarNilai{MahasiswaID=2,MataKuliahID=2050,IndexNilai=IndexNilai.C},
                new DaftarNilai{MahasiswaID=2,MataKuliahID=3050,IndexNilai=IndexNilai.B},
                new DaftarNilai{MahasiswaID=2,MataKuliahID=4050,IndexNilai=IndexNilai.A},
                new DaftarNilai{MahasiswaID=3,MataKuliahID=1050},
                new DaftarNilai{MahasiswaID=4,MataKuliahID=1050,},
                new DaftarNilai{MahasiswaID=5,MataKuliahID=5050,IndexNilai=IndexNilai.C},
                new DaftarNilai{MahasiswaID=6,MataKuliahID=4050,IndexNilai=IndexNilai.B},
            };
            daftarnilais.ForEach(s => context.DaftarNilais.Add(s));
            context.SaveChanges();
        }
    }
}