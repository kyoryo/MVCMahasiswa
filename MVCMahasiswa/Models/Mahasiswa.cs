﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MVCMahasiswa.Models
{
    public class Mahasiswa
    {
        public int MahasiswaID { get; set; }

        [Display(Name = "Nama Mahasiswa")]
        public string NamaMahasiswa { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Tanggal Masuk")]
        public DateTime TglMasuk { get; set; }

        

        [Display(Name="Daftar Nilai")]
        public virtual ICollection<DaftarNilai> DaftarNilais { get; set; }
    }
}