﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVCMahasiswa.Models
{
    public class MataKuliah
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MataKuliahID { get; set; }

        [Display(Name = "Nama Mata kuliah")]
        public string NamaMataKuliah { get; set; }

        public int Bobot { get; set; }

        public virtual ICollection<DaftarNilai> DaftarNilais { get; set; }
    }
}