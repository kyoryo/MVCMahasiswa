﻿
namespace MVCMahasiswa.Models
{
    public enum IndexNilai
    {
        A,B,C,D,E
    }
    public class DaftarNilai
    {
        public int DaftarNilaiID { get; set; }
        public int MataKuliahID { get; set; }
        public int MahasiswaID { get; set; }
        public IndexNilai? IndexNilai { get; set; }

        public virtual MataKuliah MataKuliah { get; set; }
        public virtual Mahasiswa Mahasiswa { get; set; }
    }
}