﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCMahasiswa.DAL;
using MVCMahasiswa.Models;

namespace MVCMahasiswa.Controllers
{
    public class DaftarNilaiController : Controller
    {
        private SchoolContext db = new SchoolContext();

        // GET: DaftarNilai
        public ActionResult Index(string sortOrder, string searchString)
        {           

            var daftarNilais = db.DaftarNilais.Include(d => d.Mahasiswa).Include(d => d.MataKuliah);

            ///sorting////
            ViewBag.currentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.NameMKSortParm = String.IsNullOrEmpty(sortOrder) ? "namemk_desc" : "";
            ViewBag.IndexNilSortParm = String.IsNullOrEmpty(sortOrder) ? "indexnil_desc": "";

            switch (sortOrder)
            {
                case "name_desc":
                    daftarNilais = daftarNilais.OrderByDescending(s => s.Mahasiswa.NamaMahasiswa);
                    break;
                case "namemk_desc":
                    daftarNilais = daftarNilais.OrderByDescending(s => s.MataKuliah.NamaMataKuliah);
                    break;
                case "indexnil_desc":
                    daftarNilais = daftarNilais.OrderByDescending(s => s.IndexNilai);
                    break;
                default:
                    daftarNilais = daftarNilais.OrderBy(s => s.Mahasiswa.NamaMahasiswa);
                    break;
            }
            ///search///
            if (!String.IsNullOrEmpty(searchString))
            {
                daftarNilais = daftarNilais.Where(s => s.Mahasiswa.NamaMahasiswa.Contains(searchString));
            }

            return View(daftarNilais.ToList());
        }

        // GET: DaftarNilai/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DaftarNilai daftarNilai = db.DaftarNilais.Find(id);
            if (daftarNilai == null)
            {
                return HttpNotFound();
            }
            return View(daftarNilai);
        }

        // GET: DaftarNilai/Create
        public ActionResult Create()
        {
            ViewBag.MahasiswaID = new SelectList(db.Mahasiswas, "MahasiswaID", "NamaMahasiswa");
            ViewBag.MataKuliahID = new SelectList(db.MataKuliahs, "MataKuliahID", "NamaMataKuliah");
            return View();
        }

        // POST: DaftarNilai/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DaftarNilaiID,MataKuliahID,MahasiswaID,IndexNilai")] DaftarNilai daftarNilai)
        {
            if (ModelState.IsValid)
            {
                db.DaftarNilais.Add(daftarNilai);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            ViewBag.MahasiswaID = new SelectList(db.Mahasiswas, "MahasiswaID", "NamaMahasiswa", daftarNilai.MahasiswaID);
            ViewBag.MataKuliahID = new SelectList(db.MataKuliahs, "MataKuliahID", "NamaMataKuliah", daftarNilai.MataKuliahID);
            return View(daftarNilai);
        }

        // GET: DaftarNilai/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DaftarNilai daftarNilai = db.DaftarNilais.Find(id);
            if (daftarNilai == null)
            {
                return HttpNotFound();
            }
            ViewBag.MahasiswaID = new SelectList(db.Mahasiswas, "MahasiswaID", "NamaMahasiswa", daftarNilai.MahasiswaID);
            ViewBag.MataKuliahID = new SelectList(db.MataKuliahs, "MataKuliahID", "NamaMataKuliah", daftarNilai.MataKuliahID);
            return View(daftarNilai);
        }

        // POST: DaftarNilai/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DaftarNilaiID,MataKuliahID,MahasiswaID,IndexNilai")] DaftarNilai daftarNilai)
        {
            if (ModelState.IsValid)
            {
                db.Entry(daftarNilai).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MahasiswaID = new SelectList(db.Mahasiswas, "MahasiswaID", "NamaMahasiswa", daftarNilai.MahasiswaID);
            ViewBag.MataKuliahID = new SelectList(db.MataKuliahs, "MataKuliahID", "NamaMataKuliah", daftarNilai.MataKuliahID);
            return View(daftarNilai);
        }

        // GET: DaftarNilai/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DaftarNilai daftarNilai = db.DaftarNilais.Find(id);
            if (daftarNilai == null)
            {
                return HttpNotFound();
            }
            return View(daftarNilai);
        }

        // POST: DaftarNilai/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DaftarNilai daftarNilai = db.DaftarNilais.Find(id);
            db.DaftarNilais.Remove(daftarNilai);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
