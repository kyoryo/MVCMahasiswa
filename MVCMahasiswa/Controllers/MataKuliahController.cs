﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCMahasiswa.DAL;
using MVCMahasiswa.Models;
using MVCMahasiswa.CustomFilters;

namespace MVCMahasiswa.Controllers
{
    public class MataKuliahController : Controller
    {
        private SchoolContext db = new SchoolContext();

        // GET: MataKuliah
        public ActionResult Index(string searchString, string sortOrder)
        {
            var matakuliah = from s in db.MataKuliahs
                             select s;

            ///sort///
            ViewBag.currentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            
            switch (sortOrder)
            {
                case "name_desc":
                    matakuliah = matakuliah.OrderByDescending(s => s.NamaMataKuliah);
                    break;
                default:
                    matakuliah = matakuliah.OrderBy(s => s.NamaMataKuliah);
                    break;
            }

            ////search////
            

            if (!String.IsNullOrEmpty(searchString))
            {
                matakuliah = matakuliah.Where(s => s.NamaMataKuliah.Contains(searchString));
            }

            return View(matakuliah.ToList());
            //return View(db.MataKuliahs.ToList());
        }

        // GET: MataKuliah/Details/5
        [AuthLog(Roles="Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MataKuliah mataKuliah = db.MataKuliahs.Find(id);
            if (mataKuliah == null)
            {
                return HttpNotFound();
            }
            return View(mataKuliah);
        }

        // GET: MataKuliah/Create
        [AuthLog(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: MataKuliah/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MataKuliahID,NamaMataKuliah,Bobot")] MataKuliah mataKuliah)
        {
            if (ModelState.IsValid)
            {
                db.MataKuliahs.Add(mataKuliah);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mataKuliah);
        }

        // GET: MataKuliah/Edit/5
        [AuthLog(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MataKuliah mataKuliah = db.MataKuliahs.Find(id);
            if (mataKuliah == null)
            {
                return HttpNotFound();
            }
            return View(mataKuliah);
        }

        // POST: MataKuliah/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MataKuliahID,NamaMataKuliah,Bobot")] MataKuliah mataKuliah)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mataKuliah).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mataKuliah);
        }

        // GET: MataKuliah/Delete/5
        [AuthLog(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MataKuliah mataKuliah = db.MataKuliahs.Find(id);
            if (mataKuliah == null)
            {
                return HttpNotFound();
            }
            return View(mataKuliah);
        }

        // POST: MataKuliah/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MataKuliah mataKuliah = db.MataKuliahs.Find(id);
            db.MataKuliahs.Remove(mataKuliah);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
