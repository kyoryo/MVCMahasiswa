﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCMahasiswa.DAL;
using MVCMahasiswa.Models;
using PagedList;
using MVCMahasiswa.CustomFilters;

namespace MVCMahasiswa.Controllers
{
    public class MahasiswaController : Controller
    {
        private SchoolContext db = new SchoolContext();

        // GET: Mahasiswa
        public ActionResult Index(string sortOrder, string searchString, string currentFilter, int? page)
        {
            ViewBag.currentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else { searchString = currentFilter; }

            ViewBag.currentFilter = searchString;

            var mahasiswas = from s in db.Mahasiswas
                             select s;

            if (!String.IsNullOrEmpty(searchString))
            {
                mahasiswas = mahasiswas.Where(s => s.NamaMahasiswa.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    mahasiswas = mahasiswas.OrderByDescending(s => s.NamaMahasiswa);
                    break;
                case "Date":
                    mahasiswas = mahasiswas.OrderBy(s => s.TglMasuk);
                    break;
                case "date_desc":
                    mahasiswas = mahasiswas.OrderByDescending(s => s.TglMasuk);
                    break;
                default:
                    mahasiswas = mahasiswas.OrderBy(s => s.NamaMahasiswa);
                    break;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            
            return View(mahasiswas.ToPagedList(pageNumber, pageSize));
        }

        // GET: Mahasiswa/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mahasiswa mahasiswa = db.Mahasiswas.Find(id);
            if (mahasiswa == null)
            {
                return HttpNotFound();
            }
            return View(mahasiswa);
        }

        // GET: Mahasiswa/Create
        [AuthLog(Roles="Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Mahasiswa/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NamaMahasiswa,TglMasuk")] Mahasiswa mahasiswa)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Mahasiswas.Add(mahasiswa);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /*DEX*/)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            return View(mahasiswa);
        }

        // GET: Mahasiswa/Edit/5
        [AuthLog(Roles="Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mahasiswa mahasiswa = db.Mahasiswas.Find(id);
            if (mahasiswa == null)
            {
                return HttpNotFound();
            }
            return View(mahasiswa);
        }

        // POST: Mahasiswa/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MahasiswaID,NamaMahasiswa,TglMasuk")] Mahasiswa mahasiswa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mahasiswa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mahasiswa);
        }

        // GET: Mahasiswa/Delete/5
        [AuthLog(Roles="Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mahasiswa mahasiswa = db.Mahasiswas.Find(id);
            if (mahasiswa == null)
            {
                return HttpNotFound();
            }
            return View(mahasiswa);
        }

        // POST: Mahasiswa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Mahasiswa mahasiswa = db.Mahasiswas.Find(id);
            db.Mahasiswas.Remove(mahasiswa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
