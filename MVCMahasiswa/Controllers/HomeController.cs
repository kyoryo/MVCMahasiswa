﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCMahasiswa.DAL;
using MVCMahasiswa.ViewModels;

namespace MVCMahasiswa.Controllers
{
    public class HomeController : Controller
    {
        private SchoolContext db = new SchoolContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            IQueryable<TglMasukGroup> data = from mahasiswa in db.Mahasiswas
                                             group mahasiswa by mahasiswa.TglMasuk into dateGroup
                                             select new TglMasukGroup()
                                             {
                                                 TglMasuk = dateGroup.Key,
                                                 MahasiswaCount = dateGroup.Count()
                                             };
            //default//ViewBag.Message = "Your application description page.";

            return View(data.ToList());
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}