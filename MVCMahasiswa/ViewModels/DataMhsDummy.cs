﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MVCMahasiswa.ViewModels
{
    public class DataMhsDummy
    {
        [Display(Name = "Nama Mahasiswa")]
        public string NamaMahasiswa { get; set; }

        public DateTime TglMasuk { get; set; }
    }
}