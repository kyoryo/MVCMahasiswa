﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCMahasiswa.ViewModels
{
    public class TglMasukGroup
    {
        [DataType(DataType.Date)]
        public DateTime? TglMasuk{get;set;}
        public int MahasiswaCount {get; set;}

    }
}