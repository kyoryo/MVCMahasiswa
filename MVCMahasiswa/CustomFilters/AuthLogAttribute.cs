﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCMahasiswa.CustomFilters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthLogAttribute : AuthorizeAttribute
    {        
        public AuthLogAttribute()
        {
            View = "AuthorizeFailed";
        }
        public string View { get; set; }

        //check Authorization
        //<param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            IsUserAuthorized(filterContext);
        }

        /// <summary>
        /// Method to check if the user is Authorized or not
        /// if yes continue to perform the action else redirect to error page
        /// </summary>
        /// <param name="filterContext"></param>
        private void IsUserAuthorized(AuthorizationContext filterContext)
        {
            //if result null then user authorized
            if (filterContext.Result == null)
                return;

            //if user unauthorized then navigate to authfailed-VIEW
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                var vr = new ViewResult();
                vr.ViewName = View;

                ViewDataDictionary dict = new ViewDataDictionary();
                dict.Add("Message", "Maaf anda tidak diperkenankan mengakses halaman ini.");

                vr.ViewData = dict;
                var result = vr;

                filterContext.Result = result;
            }
        }

        
    }
}